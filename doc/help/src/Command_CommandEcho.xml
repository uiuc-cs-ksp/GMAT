<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0" xml:id="CommandEcho" 
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:xi="http://www.w3.org/2001/XInclude"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:m="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <indexterm>
     <primary>CommandEcho</primary>
  </indexterm>
   
  <refmeta>
    <refentrytitle>CommandEcho</refentrytitle>

    <refmiscinfo class="source">GMAT</refmiscinfo>

    <refmiscinfo class="manual">Commands</refmiscinfo>
  </refmeta>

  <refnamediv>
    <refname>CommandEcho</refname>

    <refpurpose>Toggle the use of the <guilabel>Echo</guilabel>
    command</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Script Syntax</title>

    <synopsis><literal>CommandEcho</literal> <replaceable>EchoSetting</replaceable>   </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Description</title>
	
	<para>The <guilabel>EchoCommand</guilabel> command is used to toggle the
	use of the <guilabel>Echo</guilabel> on and off throughout a mission
	sequence. This allows for specific parts of a mission sequence to be
	displayed to the message window and the generated log file. This command
	is a part of the <guilabel>ScriptTools</guilabel> plugin.</para>
	
  </refsection>
  
  <refsection>
    <title>Options</title>
	
	<informaltable colsep="0" frame="topbot">
	  <tgroup cols="2">
	    <colspec colnum="1" colwidth="1*"/>
		
		<colspec colnum="2" colwidth="3*"/>
		
		<thead>
		  <row>
		    <entry>Option</entry>
			
			<entry>Description</entry>
		  </row>
		</thead>
		
		<tbody>
		  <row>
		    <entry><guilabel>EchoSetting</guilabel></entry>
			
			<entry><para>Specifies whether the current
			<guilabel>EchoSetting</guilabel> of the <guilabel>Echo</guilabel>
			command should be on or off.</para>
			
			<variablelist>
			    <varlistentry>
			      <term>Accepted Data Types</term>
				
				  <listitem>
				    <para>String</para>
				  </listitem>
				</varlistentry>
				
				<varlistentry>
				  <term>Allowed Values</term>
				  
				  <listitem>
				    <para>On, Off</para>
				  </listitem>
				</varlistentry>
				
				<varlistentry>
				  <term>Default Value</term>
				  
				  <listitem>
				    <para>Off</para>
				  </listitem>
				</varlistentry>
				
				<varlistentry>
				  <term>Required</term>
				  
				  <listitem>
				    <para>yes</para>
				  </listitem>
				</varlistentry>
				
				<varlistentry>
				  <term>Interfaces</term>
				  
				  <listitem>
				    <para>GUI, script</para>
				  </listitem>
				</varlistentry>
			  </variablelist></entry>
	      </row>
		</tbody>
	  </tgroup>
	</informaltable>
  </refsection>

  <refsection>
    <title>GUI</title>
	
	<para>The <guilabel>CommandEcho</guilabel> command to toggle the
	<guilabel>Echo</guilabel> on or off at any point in a mission sequence.
	Any number of this command can be placed throughout a mission sequence. 
	The message box shown below will appear when setting the
	<guilabel>EchoSetting</guilabel> through the GUI. To set the command on,
	simply replace Off with On in the text. Note that if the command is
	renamed, the new name will appear in this GUI display with quotation
	marks surrounding it.</para>
	
	<screenshot>
      <mediaobject>
        <imageobject>
          <imagedata align="center" contentdepth="100%"
                     fileref="files/images/Command_CommandEcho_GUI.PNG"
                     scalefit="1" width="100%"/>
        </imageobject>
      </mediaobject>
    </screenshot>
  </refsection>
</refentry>  