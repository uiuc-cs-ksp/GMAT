//------------------------------------------------------------------------------
//                           Sensor
//------------------------------------------------------------------------------
// GMAT: General Mission Analysis Tool.
//
// Copyright (c) 2002 - 2017 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration.
// All Other Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
// http://www.apache.org/licenses/LICENSE-2.0.
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.   See the License for the specific language
// governing permissions and limitations under the License.
//
// Author: Wendy Shoan, NASA/GSFC
// Created: 2016.05.02
//
/**
 * Implementation of the Sensor class
 */
//------------------------------------------------------------------------------

#include "gmatdefs.hpp"
#include "Sensor.hpp"

//------------------------------------------------------------------------------
// static data
//------------------------------------------------------------------------------
// None

//------------------------------------------------------------------------------
// public methods
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Sensor(Real fov)
//------------------------------------------------------------------------------
/**
 * Constructor
 * 
 * @param fov field-of-view for the sensor (radians)
 */
//---------------------------------------------------------------------------
Sensor::Sensor() :
   maxExcursionAngle (0.0)
{
}

//------------------------------------------------------------------------------
// Sensor(const Sensor &copy)
//------------------------------------------------------------------------------
/**
 * Copy constructor
 * 
 * @param copy object to copy
 */
//---------------------------------------------------------------------------
Sensor::Sensor(const Sensor &copy) :
   maxExcursionAngle (copy.maxExcursionAngle)
{
}

//------------------------------------------------------------------------------
// Sensor& operator=(const Sensor &copy)
//------------------------------------------------------------------------------
/**
 * The operator= for the Sensor
 * 
 * @param copy object to copy
 */
//---------------------------------------------------------------------------
Sensor& Sensor::operator=(const Sensor &copy)
{
   if (&copy == this)
      return *this;
   
   maxExcursionAngle    = copy.maxExcursionAngle;
   
   return *this;
}

//------------------------------------------------------------------------------
// ~Sensor()
//------------------------------------------------------------------------------
/**
 * Destructor
 * 
 */
//---------------------------------------------------------------------------
Sensor::~Sensor()
{
}


//------------------------------------------------------------------------------
// protected methods
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  bool CheckTargetMaxExcursionAngle(Real viewConeAngle)
//------------------------------------------------------------------------------
/**
 * Checks if the target lies inside the max excursion angle
 *
 * @param viewConeAngle  the view cone angle
 *
 * @return true if the point les inside the max excursion angle; false otherwise
 */
//---------------------------------------------------------------------------
bool Sensor::CheckTargetMaxExcursionAngle(Real viewConeAngle)
{
   // Check if the target lies inside the max excursion angle
   if (viewConeAngle < maxExcursionAngle)
      return true;
   return false;
}

