//------------------------------------------------------------------------------
//                           Sensor
//------------------------------------------------------------------------------
// GMAT: General Mission Analysis Tool.
//
// Copyright (c) 2002 - 2017 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration.
// All Other Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// You may not use this file except in compliance with the License.
// You may obtain a copy of the License at:
// http://www.apache.org/licenses/LICENSE-2.0.
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.   See the License for the specific language
// governing permissions and limitations under the License.
//
// Author: Wendy Shoan, NASA/GSFC
// Created: 2017.03.21
//
/**
 * Definition of the base Sensor class.  This class models a sensor.
 */
//------------------------------------------------------------------------------
#ifndef Sensor_hpp
#define Sensor_hpp

#include "gmatdefs.hpp"
//#include "OrbitState.hpp"
//#include "AbsoluteDate.hpp"

class Sensor
{
public:
   
   // class methods
   Sensor();
   Sensor( const Sensor &copy);
   Sensor& operator=(const Sensor &copy);
   
   virtual ~Sensor();
   
   virtual bool  CheckTargetVisibility(Real viewConeAngle,
                                       Real viewClockAngle = 0.0) = 0;
   
protected:
   
   // The maximum excursion angle
   Real          maxExcursionAngle;
   
   virtual bool  CheckTargetMaxExcursionAngle(Real viewConeAngle);

};
#endif // Sensor_hpp
